'use strict'

let listOfContacts, currentContact, indexOfCurrentContact, firstCount, secondCount;

$(function() {
    loadContacts();
    processTheListOfContacts();
    getEventOnClick();
});

const getEventOnClick = () => { //Event binding
    getEventOnClickContact();
    getEventOnLoadImg();

    $('#search_line').keyup(findContact);
    $('#cancel_search').click(cancelSearchContacts);
    $('#edit').click(editContactInfo);
    $('#save_changes').click(saveContactInfo);
    $('#discard_changes').click(discardChanges);
    $('#change_favorite').click(changeFavorite);
    $('#close, #overlay').click(closeModalWindow);
    $('#address_list, #company_list, #history_list, #posts_list').click(function() { showList(this); });
}

const getEventOnLoadImg = () => { //Replace an image with a download error
    $('img').on('error', function() {
        $(this).attr('src', 'full.jpg');
    });
}

const findContact = () => { //Contact search
    let substring = $('#search_line').val();

    $('.contact').each(function() { //Comparing the entered string with the names
        let string = $(this).find('.name').text().toLowerCase();

        if (!string.startsWith(substring)) {
            $(this).css('display', 'none');
        } else {
            $(this).css('display', 'flex');
        }
    })
}

const cancelSearchContacts = () => { //Cancel contact search
    $('#search_line').val('');
    $('.contact').css('display', 'flex');
}

const getEventOnClickContact = () => { //Process clicking on a contact
    $('.contact').on('click', function() { getFullInfo(this); });
}

const changeFavorite = () => { //Move contact to/from favorite
    if ($('#change_favorite').val() === '☆') {
        $('#change_favorite').val('★');
        $('#change_favorite').attr('title', 'From favorites');
    } else if ($('#change_favorite').val() === '★') {
        $('#change_favorite').val('☆');
        $('#change_favorite').attr('title', 'To favorites');
    }

    listOfContacts[indexOfCurrentContact].favorite = !currentContact.favorite;
    refreshContacts();
}

const saveContactInfo = () => { //Save contact data changes
    $('#edit').css('display', 'block');
    $('#changes').css('display', 'none');
    $('.editable').attr('contenteditable', 'false');

    currentContact.username = $('#nickname').html();
    currentContact.name = $('#fullname').html();
    currentContact.phone = $('#phone').html();
    currentContact.email = $('#email').html();
    currentContact.website = $('#website').html();

    currentContact.address.country = $('#country').html();
    currentContact.address.state = $('#state').html();
    currentContact.address.city = $('#city').html();
    currentContact.address.streetA = $('#streetA').html();
    currentContact.address.streetB = $('#streetB').html();
    currentContact.address.streetC = $('#streetC').html();
    currentContact.address.streetD = $('#streetD').html();
    currentContact.address.zipcode = $('#zipcode').html();
    currentContact.address.geo.lat = $('#lat').html();
    currentContact.address.geo.lng = $('#lng').html();

    currentContact.company.name = $('#company_name').html();
    currentContact.company.catchPhrase = $('#catchPhrase').html();
    currentContact.company.bs = $('#bs').html();

    listOfContacts[indexOfCurrentContact] = currentContact;
    sortArray(listOfContacts);
    refreshContacts();
}

const discardChanges = () => { //Discard contact data changes
    $('#edit').css('display', 'block');
    $('.editable').attr('contenteditable', 'false');
    $('#changes').css('display', 'none');
    getFullInfo();
}

const refreshContacts = () => { //Update the contact list
    localStorage.setItem('contacts', JSON.stringify(listOfContacts));
    $('.category').remove();
    processTheListOfContacts();
    getEventOnClickContact();
    getEventOnLoadImg();
}

const editContactInfo = () => { //Edit information about contact
    $('#edit').css('display', 'none');
    $('#changes').css('display', 'flex');
    $('.editable').attr('contenteditable', 'true');
}

const showList = (element) => { //Show/hide category of the information about contact
    if ($(element).html() === 'Address ▶') {
        $('.full_address').css('display', 'flex');
        $(element).html('Address ▼');
    } else if ($(element).html() === 'Address ▼') {
        $('.full_address').css('display', 'none');
        $(element).html('Address ▶');
    } else if ($(element).html() === 'Company ▶') {
        $('.full_company').css('display', 'flex');
        $(element).html('Company ▼');
    } else if ($(element).html() === 'Company ▼') {
        $('.full_company').css('display', 'none');
        $(element).html('Company ▶');
    } else if ($(element).html() === 'History ▶') {
        $('.full_history').css('display', 'block');
        $(element).html('History ▼');
    } else if ($(element).html() === 'History ▼') {
        $('.full_history').css('display', 'none');
        $(element).html('History ▶');
    } else if ($(element).html() === 'Posts ▶') {
        $('.full_posts').css('display', 'block');
        $(element).html('Posts ▼');
    } else if ($(element).html() === 'Posts ▼') {
        $('.full_posts').css('display', 'none');
        $(element).html('Posts ▶');
    }
}

const closeModalWindow = () => { //Close a window with full information about contact
    $('#overlay, #modal_window').css('visibility', 'hidden');
    $('#full_avatar').attr('src', '');
    $('#edit').css('display', 'block');
    $('.editable').attr('contenteditable', 'false');
    $('#changes, .full_address, .full_company, .full_history, .full_posts').css('display', 'none');
}

const openModalWindow = () => { //Open a window with full information about contact
    $('#modal_window, #overlay').css('visibility', 'visible');
}

const getFullInfo = (element) => { //Get all information about contact
    if (element) { //After opening the modal window
        indexOfCurrentContact = getElementIndex(element);
        currentContact = listOfContacts[indexOfCurrentContact];
        openModalWindow();
        $('#address_list').html('Address ▶');
        $('#company_list').html('Company ▶');
        $('#history_list').html('History ▶');
        $('#posts_list').html('Posts ▶');
    }

    if (currentContact.favorite) {
        $('#change_favorite').val('★');
        $('#change_favorite').attr('title', 'From favorites');
    } else {
        $('#change_favorite').val('☆');
        $('#change_favorite').attr('title', 'To favorites');
    }
    $('#full_avatar').attr('src', currentContact.avatar);
    $('#fullname').html(currentContact.name);
    $('#nickname').html(currentContact.username);
    $('#phone').html(currentContact.phone);
    $('#email').html(currentContact.email);
    $('#website').html(currentContact.website);

    $('#country').html(currentContact.address.country);
    $('#state').html(currentContact.address.state);
    $('#city').html(currentContact.address.city);
    $('#streetA').html(currentContact.address.streetA);
    $('#streetB').html(currentContact.address.streetB);
    $('#streetC').html(currentContact.address.streetC);
    $('#streetD').html(currentContact.address.streetD);
    $('#zipcode').html(currentContact.address.zipcode);
    $('#lat').html(currentContact.address.geo.lat);
    $('#lng').html(currentContact.address.geo.lng);

    $('#company_name').html(currentContactt.company.name);
    $('#catchPhrase').html(currentContact.company.catchPhrase);
    $('#bs').html(currentContact.company.bs);

    currentContact.accountHistory.forEach((element, i) => { //Filling account history
        $('<div>', { class: 'full_history date_history' }).appendTo('#account_history');
        $('<span>', { class: 'headline', text: 'Date: ' }).appendTo('.date_history:eq(' + i + ')');
        $('<span>', { text: element.date }).appendTo('.date_history:eq(' + i + ')');

        $('<div>', { class: 'full_history type_history' }).appendTo('#account_history');
        $('<span>', { class: 'headline', text: 'Type: ' }).appendTo('.type_history:eq(' + i + ')');
        $('<span>', { text: element.type }).appendTo('.type_history:eq(' + i + ')');

        $('<div>', { class: 'full_history amount_history' }).appendTo('#account_history');
        $('<span>', { class: 'headline', text: 'Amount: ' }).appendTo('.amount_history:eq(' + i + ')');
        $('<span>', { text: element.amount }).appendTo('.amount_history:eq(' + i + ')');

        $('<div>', { class: 'full_history name_history' }).appendTo('#account_history');
        $('<span>', { class: 'headline', text: 'Name: ' }).appendTo('.name_history:eq(' + i + ')');
        $('<span>', { text: element.name }).appendTo('.name_history:eq(' + i + ')');

        $('<div>', { class: 'full_history business_history' }).appendTo('#account_history');
        $('<span>', { class: 'headline', text: 'Business: ' }).appendTo('.business_history:eq(' + i + ')');
        $('<span>', { text: element.business }).appendTo('.business_history:eq(' + i + ')');

        $('<div>', { class: 'full_history history_account' }).appendTo('#account_history');
        $('<span>', { class: 'headline', text: 'Account: ' }).appendTo('.history_account:eq(' + i + ')');
        $('<span>', { text: element.account }).appendTo('.history_account:eq(' + i + ')');

        $('<div>', { class: 'line full_history' }).appendTo('#account_history');
        $('<br>', { class: 'full_history' }).appendTo('#account_history');
    });
    $('<hr>', { class: 'full_history' }).insertAfter('#history_list');

    currentContact.posts.forEach((element, i) => { //Filling account posts
        $('<div>', { class: 'full_posts paragraph_posts' }).appendTo('#account_posts');
        $('<span>', { class: 'headline', text: 'Paragraph: ' }).appendTo('.paragraph_posts:eq(' + i + ')');
        $('<span>', { text: '"' + element.paragraph + '"' }).appendTo('.paragraph_posts:eq(' + i + ')');

        $('<div>', { class: 'full_posts hedline_posts' }).appendTo('#account_posts');
        $('<span>', { class: 'headline', text: 'Headline: ' }).appendTo('.hedline_posts:eq(' + i + ')');
        $('<span>', { text: '"' + element.sentence + '"' }).appendTo('.hedline_posts:eq(' + i + ')');

        $('<div>', { class: 'full_posts text_posts' }).appendTo('#account_posts');
        $('<span>', { class: 'headline', text: 'Text: ' }).appendTo('.text_posts:eq(' + i + ')');
        $('<span>', { text: '"' + element.sentences + '"' }).appendTo('.text_posts:eq(' + i + ')');

        $('<div>', { class: 'full_posts words_posts' }).appendTo('#account_posts');
        $('<span>', { class: 'headline', text: 'Text: ' }).appendTo('.words_posts:eq(' + i + ')');
        $('<span>', { text: '("' + element.words.join('", "') + '")' }).appendTo('.words_posts:eq(' + i + ')');

        $('<div>', { class: 'line full_posts' }).appendTo('#account_posts');
        $('<br>', { class: 'full_posts' }).appendTo('#account_posts');
    });
    $('<hr>', { class: 'full_posts' }).insertAfter('#posts_list');
}

const getElementIndex = (element) => { //Get element index
    for (let i = 0; i < listOfContacts.length; i++) {
        if (listOfContacts[i].name === $(element).find('.name').html()) {
            return i;
        }
    }
}

const loadContacts = () => { //Download the contact list from the server
    $.get('http://demo.sibers.com/users').done((data) => {
        localStorage.setItem('contacts', JSON.stringify(data));
    });
}

const processTheListOfContacts = () => { //Process an array of contacts
    listOfContacts = JSON.parse(localStorage.getItem('contacts'));
    sortArray(listOfContacts);
    listOfContacts.forEach((element, i, listOfContacts) => {
        fillContacts(element, i, listOfContacts);
    });
}

const sortArray = (array) => { //Sort an array of contacts in alphabetical order
    array.sort((a, b) => {
        const x = a.name.toLowerCase();
        const y = b.name.toLowerCase();
        return x.localeCompare(y, { ignorePunctuation: true }); //Contact name comparison
    });
}

const fillContacts = (element, i, listOfContacts) => { //Fill in the contact list
    let currentString = element.name.toLowerCase();
    let prevString, indexOfTheInserted, category;
    if (i !== 0) prevString = listOfContacts[i - 1].name[0].toLowerCase();

    if (element.favorite) {
        if (!$('div').is('#favorite')) { //Add contacts to favorites
            $('<div>', { id: 'favorite', class: 'category' }).prependTo('#list_of_contacts');
            $('<div>', { class: 'category_name', text: 'Favorite' }).appendTo('#favorite');
            $('<hr>').appendTo('#favorite');
            firstCount = 0;
        }
        indexOfTheInserted = firstCount++;
        category = '#favorite';
    } else if (i === 0 || currentString.localeCompare(prevString) > 0) { //Add contacts to the appropriate letter category
        if (!$('div').is('#' + currentString[0])) {
            $('<div>', { id: currentString[0], class: 'category' }).appendTo('#list_of_contacts');
            $('<div>', { class: 'category_name', text: currentString[0].toUpperCase() }).appendTo('#' + currentString[0]);
            $('<hr>').appendTo('#' + currentString[0]);
            secondCount = 0;
        }
        indexOfTheInserted = secondCount++;
        category = '#' + currentString[0];
    }

    $('<div>', { class: 'contact' }).appendTo(category);
    $('<img>', { class: 'avatar', src: element.avatar }).appendTo(category + ' .contact:eq(' + indexOfTheInserted + ')');
    $('<div>', { class: 'short_info' }).appendTo(category + ' .contact:eq(' + indexOfTheInserted + ')');
    $('<div>', { class: 'name', text: element.name }).appendTo(category + ' .short_info:eq(' + indexOfTheInserted + ')');
    $('<div>', { class: 'nick', text: element.username }).appendTo(category + ' .short_info:eq(' + indexOfTheInserted + ')');
}